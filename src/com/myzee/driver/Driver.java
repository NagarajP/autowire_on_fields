package com.myzee.driver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.myzee.beans.TextEditor;

public class Driver {
	public static void main(String[] args) {
		ApplicationContext ap = new ClassPathXmlApplicationContext("com/myzee/resources/spring.xml");
		TextEditor t = (TextEditor)ap.getBean("t");
		t.checkSpell();
		
	}
}
