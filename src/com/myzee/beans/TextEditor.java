package com.myzee.beans;

import org.springframework.beans.factory.annotation.Autowired;

public class TextEditor {
	@Autowired
	private SpellChecker spellChecker;
	private String word;
	
	public void setWord(String word) {
		this.word = word;
	}
	
	public void checkSpell() {
		spellChecker.spellingCheck(word);
	}
	
}
